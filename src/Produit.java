class Produit {
    private int code;
    private String libelle;
    private double prixHT;
    private int cpt;

    public Produit(int code, String libelle, double prixHT, int cpt) {
        this.code = code;
        this.libelle = libelle;
        this.prixHT = prixHT;
        this.cpt = cpt;
    }
