
public class Particulier extends Client{
    private String prenom;

    public Particulier(String nom, String adresse, String prenom)
    {
        super(nom, adresse);
        this.prenom = prenom;
    }
}