
public class TypeProduit {
    private String libelle;
    private Double tauxTVA;

    public TypeProduit(String libelle, Double tauxTVA){
        this.libelle = libelle;
        this.tauxTVA = tauxTVA;
    }

    public String getLibelle(){
        return libelle;
    }

    public Double getTauxTVA(){
        return getTauxTVA;
    }
}
