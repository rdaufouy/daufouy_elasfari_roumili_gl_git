public class LectureFichierProduits {

    public static Set<Produit> creerEnsembleProduits(String cheminFichier) {
        Set<Produit> ensembleProduits = new HashSet<>();

        try (BufferedReader br = new BufferedReader(new FileReader(cheminFichier))) {
            String ligne;

            while ((ligne = br.readLine()) != null) {
                String[] elements = ligne.split(";"); // Assumant que les éléments sont séparés par des points-virgules

                if (elements.length >= 3) {
                    String nom = elements[0];
                    String description = elements[1];
                    double prix = Double.parseDouble(elements[2]);

                    Produit produit = new Produit(nom, description, prix);
                    ensembleProduits.add(produit);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ensembleProduits;
    }

    public static void main(String[] args) {
        String cheminFichier = "chemin/vers/le/fichier.txt";
        Set<Produit> produits = creerEnsembleProduits(cheminFichier);

        // Faire quelque chose avec l'ensemble de produits
        for (Produit produit : produits) {
            System.out.println(produit);
        }
    }
}