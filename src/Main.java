
public class Main {

	public static void main(String[] args) {
		Entreprise ent = new Entreprise("Entreprise_1", "La Rochelle", 1234);
		Particulier part = new Particulier("Didier", "Niort", "Martin");
		
		Facture f1 = new Facture(ent, "30-05-2023");
		Facture f2 = new Facture(part, "31-05-2023");
		Facture f3 = new Facture(part, "29-05-2023");
		
		TypeProduit type1 = new TypeProduit("Fruit", 0.055);
		TypeProduit type2 = new TypeProduit("Librairie", 0.129);
		
		Produit pomme = new Produit("Pomme", 0.5, type1);
		Produit livre = new Produit("roman_1", 19.99, type2);
		
		f1.ajouterProduit(pomme);
		f1.ajouterProduit(pomme);
		f1.ajouterProduit(livre);
		
		f2.ajouterProduit(pomme);
		f2.ajouterProduit(livre);
		
		f3.ajouterProduit(livre);
		
		if(f1.montantTTC() != 23.62371) {
			System.err.println("Erreur sur le calcul du montant TTC de la facture f1");
		}
		if(f2.montantTTC() != 23.09621) {
			System.err.println("Erreur sur le calcul du montant TTC de la facture f2");
		}
		if(f3.montantTTC() != 22.56871) {
			System.err.println("Erreur sur le calcul du montant TTC de la facture f3");
		}
	}

}
