import java.io.FileWriter;
import java.io.IOException;

public class SauvegardeClients {

    public static void main(String[] args) {
        genererFichierProduits();
        genererFichierClients();
    }

    public static void genererFichierProduits() {
        try {
            FileWriter writer = new FileWriter("fichier_produits.txt");
            writer.write("1;Pomme;0.35;Fruit;0.055\n");
            writer.write("2;PS5;750;Console;0.195\n");
            writer.close();
            System.out.println("Fichier produits généré avec succès.");
        } catch (IOException e) {
            System.out.println("Erreur lors de la génération du fichier produits : " + e.getMessage());
        }
    }

    public static void genererFichierClients() {
        try {
            FileWriter writer = new FileWriter("fichier_clients.txt");
            writer.write("Martin;La Rochelle;Pierre\n");
            writer.write("SNCF;Paris;552049447\n");
            writer.close();
            System.out.println("Fichier clients généré avec succès.");
        } catch (IOException e) {
            System.out.println("Erreur lors de la génération du fichier clients : " + e.getMessage());
        }
    }
}

